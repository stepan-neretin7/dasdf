from abc import ABC, abstractmethod
import numpy as np

from activation.activation import Activation


class Layer(ABC):
    def __init__(self, input_size: int, output_size: int, activation: Activation):
        self.input_size = input_size
        self.output_size = output_size
        self.weights = np.random.randn(input_size, output_size)
        self.bias = np.zeros((1, output_size))
        self.inputs = None
        self.outputs = None
        self.d_weights = None
        self.d_bias = None
        self.activation = activation

    @abstractmethod
    def forward(self, inputs: np.ndarray) -> np.ndarray:
        pass

    @abstractmethod
    def backward(self, d_outputs: np.ndarray, learning_rate: float) -> np.ndarray:
        pass
