import numpy as np

from activation.activation import Activation


class SigmoidActivation(Activation):
    def forward(self, x: np.ndarray) -> np.ndarray:
        return 1 / (1 + np.exp(-x))

    def backward(self, inputs: np.ndarray, d_outputs: np.ndarray) -> np.ndarray:
        return d_outputs * self.forward(inputs) * (1 - self.forward(inputs))
