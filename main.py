import pandas as pd
from sklearn.preprocessing import StandardScaler

from activation.tanh_activation import TanhActivation
from layer.dense_layer import DenseLayer
from layer.layer import Layer
from network import NeuralNetwork

target = "Price"
df = pd.read_csv("data.csv")
encoded_data = pd.get_dummies(df, columns=['Brand'], prefix='Brand', drop_first=True)
X = encoded_data[["Storage_Capacity", "RAM_Size"]].values
scaler = StandardScaler()
X = scaler.fit_transform(X)
y = encoded_data[target].values.reshape(-1, 1)
y = scaler.fit_transform(y)

# Build and train the neural network
input_size = X.shape[1]
hidden_size1 = 8
output_size = 1
# Replace SigmoidActivation with TanhActivation
layer1 = DenseLayer(input_size, hidden_size1, TanhActivation())
layer2 = DenseLayer(hidden_size1, output_size, TanhActivation())

neural_network = NeuralNetwork([layer1, layer2])
learning_rate = 0.01
epochs = 1000
neural_network.fit(X, y, learning_rate, epochs)

print(neural_network.accuracy(X, y))
print(f"Real answer: {encoded_data[target][0]}")
ans = neural_network.guess(X[0])
ans = scaler.inverse_transform(ans)
print(ans[0][0])