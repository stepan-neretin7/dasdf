import numpy as np


class NeuralNetwork:
    def __init__(self, layers):
        self.layers = layers

    def forward_propagation(self, x: np.ndarray) -> np.ndarray:
        for layer in self.layers:
            x = layer.forward(x)
        return x

    def back_propagation(self, d_output: np.ndarray, learning_rate: float) -> None:
        for layer in reversed(self.layers):
            layer.backward(d_output, learning_rate)

    @staticmethod
    def mse(y_true: np.ndarray, y_pred: np.ndarray) -> float:
        return ((y_true - y_pred) ** 2).sum()

    def fit(self, x: np.ndarray, y: np.ndarray, learning_rate: float, epochs: int) -> None:
        cf = 1
        for epoch in range(epochs):
            total_error = 0
            for i in range(len(x)):
                outputs = self.forward_propagation(x[i])
                error = self.mse(y[i], outputs)
                total_error += error
                d_output = outputs - y[i]
                self.back_propagation(d_output, cf * learning_rate)
            mean_error = total_error / len(x)
            if epoch % 10 == 0:
                cf *= 0.8
            print(f'Epoch {epoch + 1}/{epochs}, Mean Squared Error: {mean_error}')

    @staticmethod
    def accuracy(y_true: np.ndarray, y_pred: np.ndarray) -> float:
        deviations = abs(y_pred - y_true)
        true_absolute = abs(y_true)
        accuracies = deviations / true_absolute
        max_accuracy = max(accuracies)
        return max_accuracy

    def guess(self, x: np.ndarray) -> np.ndarray:
        return self.forward_propagation(x)

