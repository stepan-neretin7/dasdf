import numpy as np

from activation.activation import Activation
from layer.layer import Layer


class DenseLayer(Layer):
    def __init__(self, input_size: int, output_size: int, activation: Activation):
        super().__init__(input_size, output_size, activation)

    def forward(self, inputs: np.ndarray) -> np.ndarray:
        self.inputs = inputs
        self.outputs = np.dot(inputs, self.weights) + self.bias
        return self.activation.forward(self.outputs)

    def backward(self, d_outputs: np.ndarray, learning_rate: float) -> np.ndarray:
        d_inputs = np.dot(d_outputs, self.weights.T)

        self.d_weights = np.dot(self.inputs.reshape(-1, 1), d_outputs)
        self.d_bias = d_outputs

        self.weights -= learning_rate * self.d_weights / np.sqrt(1 + (self.d_weights ** 2).sum())
        self.bias -= learning_rate * self.d_bias / np.sqrt(1 + (self.d_weights ** 2).sum())

        return d_inputs
