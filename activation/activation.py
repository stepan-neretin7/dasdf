import numpy as np
from abc import ABC, abstractmethod


class Activation(ABC):
    @abstractmethod
    def forward(self, x: np.ndarray) -> np.ndarray:
        pass

    @abstractmethod
    def backward(self, inputs: np.ndarray, d_outputs: np.ndarray) -> np.ndarray:
        pass
