import numpy as np

from activation.activation import Activation


class TanhActivation(Activation):
    def forward(self, x: np.ndarray) -> np.ndarray:
        return np.tanh(x)

    def backward(self, inputs: np.ndarray, d_outputs: np.ndarray) -> np.ndarray:
        return d_outputs * (1 - np.tanh(inputs)**2)
